FROM ruby:2.7

SHELL ["/bin/bash", "-c"]

ARG APP_ROOT=/opt/app-root

ENV APP_ROOT=$APP_ROOT \
	BUNDLE_DISABLE_PLATFORM_WARNINGS=true \
	BUNDLE_DISABLE_VERSION_CHECK=true \
	BUNDLE_USER_HOME=$GEM_HOME \
	DATA_VOLUME=/data \
	# This makes OKD happy:
	HOME=$APP_ROOT \
	LANG=en_US.UTF-8 \
	LANGUAGE=en_US:en \
	RAILS_ENV=production \
	RAILS_PORT=3000 \
	TZ=US/Eastern

COPY ./root/ /

WORKDIR $APP_ROOT

RUN set -eux; \
	curl -sL https://deb.nodesource.com/setup_12.x | bash - ; \
	apt-get -y update; \
	apt-get -y install \
	  dumb-init gosu less locales ncat nodejs postgresql-client \
	; \
	apt-get -y clean; \
	rm -rf /var/lib/apt/lists/* ; \
	npm install -g yarn; \
	mkdir -p -m 0775 $DATA_VOLUME; \
	useradd -r -u 1001 -g 0 -d $APP_ROOT -s /sbin/nologin app-user; \
	echo "$LANG UTF-8" >> /etc/locale.gen; \
	locale-gen $LANG

VOLUME $DATA_VOLUME

EXPOSE $RAILS_PORT

ENTRYPOINT ["/usr/libexec/entrypoint"]

CMD ["bundle", "exec", "rails", "server", "-b", "0.0.0.0"]

ONBUILD COPY . .

ONBUILD RUN \
	BUNDLED_WITH="$(tail -1 Gemfile.lock | tr -d ' ')"; \
	gem install "bundler:${BUNDLED_WITH:-~>2.0}"; \
	bundle install --jobs=2 --retry=2; \
	bundle exec rake tmp:create; \
	SECRET_KEY_BASE="${SECRET_KEY_BASE:-$(bundle exec rake secret)}" \
	  bundle exec rake assets:precompile 2>/dev/null \
	; \
	# Fix permissions
	chmod -R g=u . $GEM_HOME
