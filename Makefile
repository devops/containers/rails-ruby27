SHELL = /bin/bash

build_tag ?= ruby-bullseye
test_tag = $(build_tag)-test

ifdef CI_COMMIT_SHORT_SHA
	test_container = app-ruby-bullseye-$(CI_COMMIT_SHORT_SHA)
else
	test_container = app-ruby-bullseye-test
endif

.PHONY : build
build:
	DOCKER_BUILDKIT=1 docker build --pull -t $(build_tag) -f ./Dockerfile ./src

.PHONY : clean
clean:
	rm -rf ./test-app

.PHONY : test
test: test-image
	test_tag=$(test_tag) test_container=$(test_container) ./test.sh

.PHONY : test-image
test-image: test-app
	echo "FROM $(build_tag)" > ./test-app/Dockerfile
	DOCKER_BUILDKIT=1 docker build -t $(test_tag) ./test-app

.PHONY : test-app-image
test-app-image:
	DOCKER_BUILDKIT=1 docker build -t rails-test-app - < ./Dockerfile.test-app

test-app: test-app-image
	docker run --rm -d --name rails-test-app rails-test-app /bin/bash -c 'sleep infinity'
	docker cp rails-test-app:/usr/src/app ./test-app
	docker stop rails-test-app
